const Sequelize = require('sequelize');
const express = require('express');
const Joi = require('joi');
const app = express();

app.use(express.json());


const sequelize = new Sequelize('visitors', 'postgres', 'password', {
    host: 'localhost',
    dialect: 'postgres',
    operatorsAliases: false
  });


  sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  }).catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  sequelize.sync();

  const User = sequelize.define('GroupVisitor',{
    empName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    purpose: {
      type: Sequelize.STRING,
      defaultValue: 'Default Purpose'
    }
  },
  {
    timestamps: false,
    freezeTableName: true,
    validate: {

    }
  });



  app.get('/', (req, res)=> {
    return res.send('Hello World Just Hit to /api/users/ to get started....');
  });

  app.get('/api/users', (req, res)=> {

    const users = [];
    //Fetching User Database
    User.findAll().then((us)=>{
      us.forEach(element => {
        users.push(element.dataValues);        
      });
    }).then(()=>{
      if(users.length == 0) return res.send('No records, Insert Data First');
      return res.send(users);
    }).catch((err)=>{
      return res.send(`Error : ${err}`);
    });
  });


  app.get('/api/users/:id', (req, res)=> {

    //Checking Record with Given ID
    User.findAll().then((us)=>{
      const u = us.find(c=> c.dataValues.id === parseInt(req.params.id));
      if (!u) {
        return res.status(404).send('User with given ID was not found');
      } else {
        return res.send(u);
      }

    }).catch((err)=>{
      return res.send(`Error : ${err}`);
    });
  });


  app.post('/api/users', (req,res)=>{

    //Validating Data
    const {error} = validateCourse(req.body); // {error} === result.error // Its Calls Object Destructuring
    if(error) {
      return res.status(400).send(error.details[0].message);
    } else {
        const user = {
          empName: req.body.name,
          purpose: req.body.purpose
        };

        //Creating Record in Database
        User.sync().then(()=>{
          return User.create(user);
        }).then((e)=>{
          return res.send(`Data Inserted : ${e.dataValues.empName},${e.dataValues.purpose}`);  
        }).catch((err)=>{
          return res.send(`Error: ${err}`);
        });
    }

  });


  app.put('/api/users/:id', (req,res)=>{

    //Checking Record with Given ID
    User.findAll().then((us)=>{
      const u = us.find(c=> c.dataValues.id === parseInt(req.params.id));
      if (!u) {
        return res.status(404).send('User with given ID was not found');
      } else {
          //Validating Data
          const {error} = validateCoursePut(req.body); // {error} === result.error // It Calls Object Destructuring
          if(error) {
            return res.status(400).send(error.details[0].message);
          } else {
            // Valid Data
            newName = (req.body.name == null)?u.empName:req.body.name;
            newPurpose = (req.body.purpose == null)?u.purpose:req.body.purpose;
    
          const user = {
            empName: newName,
            purpose: newPurpose
          };
          User.update(
            user,
            {
              where: {id: req.params.id}
            }).then((e)=>{
              return res.send(`Database has been Updated of id: ${req.params.id}`);
            });
          }
      }

    });
      
  });


  app.delete('/api/users/:id', (req,res)=>{
      //Checking Record with Given ID
      User.findAll().then((us)=>{
        
        const u = us.find(c=> c.dataValues.id === parseInt(req.params.id));
        
        if (!u) {
          return res.status(404).send('User with given ID was not found');
        } else {
            // Deleting Data
            User.destroy(
              {where: {id: req.params.id}}
            ).then((e)=>{
              return res.send(`User with ID ${req.params.id} has been Deleted`);
            }).catch((err)=>{
              return res.send(`Error : ${err}`);
            });
        }
      }).catch((err)=>{
        return res.send(`Error : ${err}`);
      });

  });

  function validateCourse(user){
    const schema = {
      name: Joi.string().min(3).required(),
      purpose: Joi.string().min(3)
    };
    return Joi.validate(user, schema);
  }

  function validateCoursePut(user){
    const schema = {
      name: Joi.string().min(3),
      purpose: Joi.string().min(3)
    };
    return Joi.validate(user, schema);
  }

    // PORT
  const port = process.env.PORT || 3000;
  app.listen(port, ()=>{
      console.log(`Listening On Port ${port}...`);
  });